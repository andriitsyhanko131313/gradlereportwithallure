package ua.com.epam.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import ua.com.epam.utils.ReportAttachments;

public class GmailTestListener implements ITestListener {
    private static final Logger logger = LogManager.getLogger(GmailTestListener.class);

    @Override
    public void onTestStart(ITestResult iTestResult) {
        logger.info("--------------Test starts--------------------");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        logger.info(" --------------Test success----------------");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        ReportAttachments.addScreenToAllure();
        logger.info("Screenshot saved");
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        logger.info("Test skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }

    @Override
    public void onStart(ITestContext iTestContext) {
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        logger.info("-------------Test finished-------------------");
    }


}
