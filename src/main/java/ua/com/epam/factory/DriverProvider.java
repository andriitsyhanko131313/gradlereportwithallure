package ua.com.epam.factory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class DriverProvider {
    private static final Logger logger = LogManager.getLogger(DriverProvider.class);
    private static WebDriver driver;

    public static WebDriver getDriver() {
        if (driver == null) {
            driver = DriverFactory.createDriver();
        }
        return driver;
    }

    public static void quitDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
            logger.info("Driver was successfully closed");
        }
    }

    public static void closeWindow() {
        driver.close();
        logger.info("Window was successfully closed");
    }

    private DriverProvider() {
    }
}
