package ua.com.epam.decorator.elements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import ua.com.epam.decorator.PageElement;
import ua.com.epam.utils.ReportAttachments;
import ua.com.epam.utils.WaitUtils;

public class Label extends PageElement {
    private static final Logger logger = LogManager.getLogger(Label.class);

    public Label(WebElement webElement, By by) {
        super(webElement, by);
    }

    @Override
    public boolean isDisplayed() {
        ReportAttachments.logDebug("Wait element with locator to be displayed with locator", getLocator());
        WaitUtils.waitForElementPresence(getLocator());
        return this.isDisplayed();
    }

    public Label waitTextToBe(String text) {
        ReportAttachments.logDebug("Wait text to be in element with locator ", getLocator());
        WaitUtils.waitTextToBe(locator, text);
        return this;
    }
}
