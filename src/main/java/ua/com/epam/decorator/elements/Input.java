package ua.com.epam.decorator.elements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import ua.com.epam.decorator.PageElement;
import ua.com.epam.factory.DriverProvider;
import ua.com.epam.utils.ReportAttachments;
import ua.com.epam.utils.WaitUtils;

public class Input extends PageElement {
    private static final Logger logger = LogManager.getLogger(Input.class);

    public Input(WebElement webElement, By by) {
        super(webElement, by);
    }

    public void clearAndSendKeys(CharSequence... keys) {
        ReportAttachments.logDebug("Clear field an send keys [{keys}]");
        clear();
        webElement.sendKeys(keys);
    }

    public void waitForFieldReadyToInput() {
        ReportAttachments.logDebug("Wait element to be enabled with locator", getLocator());
        WaitUtils.waitForElementToBeEnabled(this);
    }
}
