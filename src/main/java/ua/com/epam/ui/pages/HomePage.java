package ua.com.epam.ui.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ua.com.epam.decorator.elements.Button;
import ua.com.epam.decorator.elements.Input;
import ua.com.epam.decorator.elements.Label;
import ua.com.epam.utils.ReportAttachments;
import ua.com.epam.utils.WaitUtils;

import static ua.com.epam.constants.Constants.*;


public class HomePage extends AbstractPage {
    private static final Logger logger = LogManager.getLogger(HomePage.class);

    @FindBy(xpath = "//a[contains(@aria-label,'Google Account:')]")
    private Label accountInformation;

    @FindBy(css = "div.T-I.T-I-KE.L3")
    private Button composeButton;

    @FindBy(xpath = "//span[@class='aT']/span[@class='bAq']")
    private Label informMessage;

    @FindBy(xpath = "//input[@placeholder='Search mail']")
    private Input searchEmail;

    public void clickComposeButton() {
        ReportAttachments.logInfo("Click compose button");
        composeButton.saveClick();
    }

    public String getAccountInformation() {
        ReportAttachments.logInfo("Get account login");
        WaitUtils.waitForPageLoadComplete();
        return accountInformation.getAttribute(ATTRIBUTE_ARIA_LABEL);
    }

    public void setSearchEmailField(CharSequence charSequence) {
        ReportAttachments.logInfo("Fill in search email field");
        searchEmail.waitForFieldReadyToInput();
        searchEmail.clearAndSendKeys(charSequence, Keys.ENTER);
    }

    public Label getInformationMessage() {
        ReportAttachments.logInfo("Get information message");
        return informMessage;
    }
}
