package ua.com.epam;

import org.testng.annotations.Test;
import ua.com.epam.asserts.LoginAsserter;
import ua.com.epam.ui.actions.LoginActions;
import ua.com.epam.ui.actions.NavigationActions;
import ua.com.epam.utils.FileManager;
import ua.com.epam.utils.User;

public class LogInToAccountTest extends BaseTest {
    @Test(description = "Verify log in to account")
    public void logInToAccountTestCase() {
        NavigationActions navigationAction = new NavigationActions();
        LoginActions loginActions = new LoginActions();
        LoginAsserter loginAsserter = new LoginAsserter();
        User user = FileManager.getUser();

        navigationAction.navigateToLoginPage();
        loginActions.logInToAccount(user.getLogin(), user.getPassword());
        loginAsserter.assertSuccessfulLogIn(user.getLogin());
    }
}
