package ua.com.epam;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import ua.com.epam.factory.DriverProvider;
import ua.com.epam.listener.GmailTestListener;


@Listeners(GmailTestListener.class)
public class BaseTest {
    @AfterMethod
    public void tearDown() {
        DriverProvider.quitDriver();
    }
}
